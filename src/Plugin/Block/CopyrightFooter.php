<?php

namespace Drupal\copyright_footer\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "copyright_footer_block",
 *   admin_label = @Translation("Copyright footer"),
 * )
 */
class CopyrightFooter extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $text = str_replace("@year", date("Y"), $this->t($this->configuration['company']));
    $text = str_replace("@sitename", \Drupal::config('system.site')->get('name'), $text);
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('copyright_footer')->getPath();
    $imageUrl = $module_path . '/img/egm_logo.svg';

    return [
      '#theme' => 'copyright_footer',
      '#text' => $text,
      '#image' => $imageUrl,
      '#attached' => [
        'library' => [
          'copyright_footer/copyright_footer',
        ],
      ],
      '#cache' => [
        'tags' => [
          'config:system.site',
        ],
        'max-age' => Cache::PERMANENT,
      ],
    ];
  }

  public function defaultConfiguration() {
    return [
      'company' => "Copyright © @year @sitename",
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
   // $config = $this->config('copyright_footer.settings');
    $form['company'] = array(
      '#type' => 'textfield',
      '#title' => t("Company"),
      '#required' => TRUE,
      '#description' => t('Side company label (use @year for current Year number, @sitename for site name)'),
      '#default_value' => $this->configuration['company'],
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
   $this->configuration['company'] = $form_state->getValue('company');
  }

}
