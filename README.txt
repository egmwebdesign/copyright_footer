Copyright Footer

FELADATAI:
- blokként megjeleníti a lábléc részt.

LEÍRÁS:
A lábléc állandó "webdesign by EGM" szövegét jelenítjük meg blokkban. Beállítható a cégnév a blokk beállítása alatt.

HASZNÁLAT
Bekapcsolás után, a blokk elrendezésnél a footerbe beszúrjuk a blokkot, majd a blokk beállításánál megadhatjuk az aktuális cég nevét.
